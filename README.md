# Regium
## Установка
```
git clone https://gitlab.com/Alexandrhhh/regiumtestwork.git regium
cd regium
composer install
cp .example.env .env
nano .env
```
Поменять настройки подключения к базе,
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

После
```
php artisan key:generate
php artisan config:cache
php artisan migrate
```

Теперь необходимо создать категорию, создаем через tinker

```
php artisan tinker
> \App\Models\Category::create(['title'=> 'Новая категория']);
> exit
```

Запускаем
```
php artisan serve
```

Обращения к api, 
```
api/articles
```

**обязательный параметр для передачи 'category', передаём числом**