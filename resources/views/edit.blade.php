@extends('layout')

@section('title')
    Добавление статьи
@endsection

@section('content')
    <form action="{{ route('articles.update', $article) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Заголовок</label>
            <input type="text" name="title" class="form-control" value="{{ $article->title }}">
        </div>
        <div class="form-group">
            <label>Категория</label>
            <select class="form-control" name="category_id">
                @foreach ($categories as $key => $category)
                    <option value="{{ $category->id }}" @if($article->category->id == $category->id) selected @endif>{{ $category->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Описание</label>
            <textarea class="form-control" name="description">{{ $article->description }}</textarea>
            <small>Необязательно</small>
        </div>
        <div class="form-group">
            <label>Изображение</label>
            @if($article->image)<img src="{{ Storage::url($article->image) }}" alt="">@endif
            <input type="file" class="form-control-file" name="image">
        </div>
        <button type="submit" class="btn btn-primary">Обновить</button>
    </form>
@endsection
