@extends('layout')

@section('title')
    Добавление статьи
@endsection

@section('content')
    <form action="{{ route('articles.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Заголовок</label>
            <input type="text" name="title" class="form-control">
        </div>
        <div class="form-group">
            <label>Категория</label>
            <select class="form-control" name="category_id">
                @foreach ($categories as $key => $category)
                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Описание</label>
            <textarea class="form-control" name="description"></textarea>
            <small>Необязательно</small>
        </div>
        <div class="form-group">
            <label>Изображение</label>
            <input type="file" class="form-control-file" name="image">
        </div>
        <button type="submit" class="btn btn-primary">Добавить</button>
    </form>
@endsection
