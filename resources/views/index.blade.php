@extends('layout')

@section('title')
    Главная
@endsection

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th>№</th>
                <th>Заголовок</th>
                <th>Категория</th>
                <th>Описание</th>
                <th>Изображение</th>
                <th></th>
            </tr>
            <tbody>
                @foreach ($articles as $key => $article)
                    <tr>
                        <td>{{ $article->id }}</td>
                        <td>{{ $article->title }}</td>
                        <td>{{ $article->category->title }}</td>
                        <td>{{ $article->description }}</td>
                        <td><img src="{{ Storage::url($article->image) }}" alt="" class="img-thumbnail" style="max-width: 150px"></td>
                        <td><a href="{{ route('articles.edit', ['article' => $article]) }}" class="btn btn-sm btn-success">Редактировать</a></td>
                    </tr>
                @endforeach
            </tbody>
        </thead>
    </table>
@endsection
