<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top mb-3">
        <a class="navbar-brand" href="#">Regium</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item @if(Route::is('articles.index')) active @endif">
                    <a class="nav-link" href="{{ route('articles.index') }}">Главная</a>
                </li>
                <li class="nav-item @if(Route::is('articles.create')) active @endif">
                    <a class="nav-link" href="{{ route('articles.create') }}">Добавить</a>
                </li>
            </ul>
        </div>
    </nav>

    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show mb-3" role="alert">
            {!! session()->get('success')!!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show mb-3" role="alert">
            {!! session()->get('error')!!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-warning fade show mb-3" role="alert">
            @foreach ($errors->all() as $error)
                {{ $error }}
            @endforeach
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    </div>
    @endif

    <main role="main" class="container">

        @yield('content')

    </main>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
