<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'description', 'image', 'category_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'id', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    static function last_5($category_id)
    {
        return \App\Models\Article::where('category_id', $category_id)->orderBy('created_at')->take(5)->get();
    }

}
