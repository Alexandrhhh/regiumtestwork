<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function last_5(Request $request)
    {
        $request->validate([
            'category' => 'required|numeric'
        ]);
        $articles = \App\Models\Article::last_5($request->category);

        if($articles->count() == 0) {
            return 'end';
        } else {
            return $articles->toJson();
        }
    }
}
